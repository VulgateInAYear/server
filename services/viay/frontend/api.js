/* API */

async function getVerses(bible, book, chapter) {
	return fetch(serverUrl + "/bibles/" + bible + "/" + book + "/" + chapter + "/verses").then(function(response) {
		return response.json()
	})
}

async function getBooks(bible) {
	return fetch(serverUrl + "/bibles/" + bible + "/books").then(function(response) {
		return response.json()
	})
}
