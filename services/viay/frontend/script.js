const serverUrl = "https://api.vulgateinayear.com/api/vx"

const readingPlan = [3, 4, 4, 4, 4, 4, 3] // Starts on Sunday

function init() {
	dayjs.extend(window.dayjs_plugin_dayOfYear)
	let dies = dayjs().dayOfYear()
	document.getElementById("dies").innerHTML = dies

	getBooks("clementine").then(function(books) {
		// Calculate the year's progress up to but not including today
		let progress = Array(books.length).fill(0)
		let currentBook = 0
		for (i = 0; i < dies - 1; i++) {
			progress[currentBook] += readingPlan[dayjs().startOf("year").add(i, "day").get("day")]
			if (progress[currentBook] > books[currentBook]["Chapters"]) {
				progress[currentBook + 1] -= books[currentBook]["Chapters"] - progress[currentBook]
				progress[currentBook] = books[currentBook]["Chapters"]
				currentBook += 1
			}
		}

		// Fetch today's readings
		let requests = []
		let offset = 0
		for (i = 0; i < readingPlan[dayjs().get("day")]; i++) {
			if (progress[currentBook] + i >= books[currentBook]["Chapters"]) {
				currentBook += 1
				offset = i
			}
			document.getElementById("readings").innerHTML += `<a href=""><p class="text-lg fade-in">${books[currentBook]["Name"]} ${progress[currentBook] + i + 1 - offset}</p></a>`
			requests.push(getVerses("clementine", books[currentBook]["Slug"], progress[currentBook] + i + 1 - offset))
		}
		Promise.all(requests).then(function(responses) {
			responses.forEach(function(response) {
				response.forEach(function(verse) {
					document.getElementById("verses").innerHTML += verseToHtml(verse)
				})
			})
		})
	})
}

function verseToHtml(verse) {
	return `<div class="hgrid grid-gap-4 w-full fade-in">
					<p class="hgrid-item-1 text-right">${verse["Number"]}</p>
					<p class="hgrid-item-11 pl-4">${verse["Content"]}</p>
			</div>`
}
