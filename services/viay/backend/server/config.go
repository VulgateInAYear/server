package server

import (
	"os"
	"encoding/json"
)

type Config struct {
	Port uint16
}

func (self *Config) LoadFromFile(path string) error {
	data, err := os.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			defaultConfigData, err := json.MarshalIndent(DefaultConfig, "", "\t")
			if err != nil { return err }

			err = os.WriteFile(path, defaultConfigData, 0755)
			if err != nil { return err }
			self = DefaultConfig
			return nil
		} else {
			return err
		}
	}

	err = json.Unmarshal(data, self)
	if err != nil { return err }
	return nil
}
