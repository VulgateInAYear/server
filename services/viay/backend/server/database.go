package server

import (
	"os"
	"strings"
	"log"
	"time"
	"context"
	"errors"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Open() (*pgxpool.Pool, error) {
	host := os.Getenv("DBHOST")
	user := os.Getenv("DBUSER")
	pass := os.Getenv("DBPASS")

	// Connect to the database
	conn, err := pgxpool.Connect(context.Background(), "host=" + host + " port=5432 user=" + user + " password=" + pass + " dbname=viay sslmode=disable")
	if err != nil { return nil, err }

	err = conn.Ping(context.Background())
	return conn, err
}

func Close(conn *pgxpool.Pool) {
	conn.Close()
}

func MigrateUp(conn *pgxpool.Pool) error {
	_, err := conn.Exec(context.Background(), "CREATE TABLE IF NOT EXISTS Migrations (Id INT PRIMARY KEY NOT NULL, Name VARCHAR(255), CreatedAt BIGINT);")
	if err != nil { return err }

	files, err := os.ReadDir("bible/migrations")
	if err != nil { return err }

	migrations := make([]string, 0)
	for _, file := range files {
		if !strings.HasSuffix(file.Name(), ".up.sql") { continue }
		migrations = append(migrations, file.Name())
	}

	latest := -1
	err = conn.QueryRow(context.Background(), "SELECT Id FROM Migrations ORDER BY Id DESC LIMIT 1;").Scan(&latest)
	if err != nil && err != pgx.ErrNoRows { return err }

	for i, filename := range migrations {
		if i <= latest { continue }

		bytes, err := os.ReadFile("bible/migrations/" + filename)
		if err != nil { return err }

		log.Println("Running Migration: ", filename)
		_, err = conn.Exec(context.Background(), string(bytes))
		if err != nil { return err }

		_, err = conn.Exec(context.Background(), "INSERT INTO migrations (Id, Name, CreatedAt) VALUES ($1, $2, $3);", i, filename, time.Now().Unix())
	}

	return nil
}

func MigrateDown(conn *pgxpool.Pool) error {
	files, err := os.ReadDir("bible/migrations")
	if err != nil { return err }

	migrations := make([]string, 0)
	for _, file := range files {
		if !strings.HasSuffix(file.Name(), ".down.sql") { continue }
		migrations = append(migrations, file.Name())
	}

	latest := -1
	err = conn.QueryRow(context.Background(), "SELECT Id FROM Migrations ORDER BY Id DESC LIMIT 1;").Scan(&latest)
	if err != nil && err != pgx.ErrNoRows { return err }
	if latest == -1 { return errors.New("Database already migrated down") }
	migrations = migrations[:latest + 1]

	for i := range migrations {
		filename := migrations[len(migrations) - 1 - i]
		bytes, err := os.ReadFile("bible/migrations/" + filename)
		if err != nil { return err }

		log.Println("Running Migration:", filename)
		_, err = conn.Exec(context.Background(), string(bytes))
		if err != nil { return err }

		_, err = conn.Exec(context.Background(), "DELETE FROM Migrations WHERE Id=$1;", len(migrations) - 1 - i)
		if err != nil { return err }
	}

	_, err = conn.Exec(context.Background(), `DROP TABLE Migrations;`)
	if err != nil { log.Fatal(err) }

	return nil
}
