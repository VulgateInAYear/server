package server

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rs/cors"

	"viay/bible"
)

type Server struct {
	*Config
}

func (self *Server) Start() {
	conn, err := Open()
	if err != nil { log.Fatal(err) }

	r := mux.NewRouter()
	api := r.PathPrefix("/api/vx").Subrouter()

	bible := &bible.Bible{
		Router: api,
		Conn: conn,
	}
	bible.Route()

	http.Handle("/", cors.Default().Handler(r))

	err = http.ListenAndServe(":" + strconv.FormatInt(int64(self.Port), 10), nil)
	if err != nil { log.Fatal(err) }
}
