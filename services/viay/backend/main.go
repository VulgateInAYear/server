package main

import (
	"log"
	"os"

	"viay/server"
)

func main() {
	config := &server.Config{}
	err := config.LoadFromFile("config.json")
	if err != nil { log.Fatal(err) }

	if len(os.Args) < 2 {
		os.Args = append(os.Args, "serve")
	}

	switch os.Args[1] {
		case "serve":
			log.Println("Starting server on port", config.Port)
			server := &server.Server{
				Config: config,
			}
			server.Start()
		case "migrate":
			switch os.Args[2] {
				case "up":
					conn, err := server.Open()
					if err != nil { log.Fatal(err) }
					err = server.MigrateUp(conn)
					if err != nil { log.Fatal(err) }
					server.Close(conn)
				case "down":
					conn, err := server.Open()
					if err != nil { log.Fatal(err) }
					err = server.MigrateDown(conn)
					if err != nil { log.Fatal(err) }
					server.Close(conn)
			}
	}
}
