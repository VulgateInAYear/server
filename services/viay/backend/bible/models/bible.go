package models

type Bible struct {
	Id int32 `json:"-"`
	Name string `json:"Name"`
	Slug string `json:"Slug"`
}

