package models

type Chapter struct {
	Id int32 `json:"-"`
	BookId int32 `json:"-"`
	Number int16 `json:"Number"`
	Name string `json:"Name"`
}


// func RetrieveChapter(bibleSlug, bookSlug string) (*models.Book, error) {
// 	row := Conn.QueryRow(context.Background(), "SELECT Id, Name, Slug FROM Books WHERE Slug=$1 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$2);", bookSlug, bibleSlug)
// 	book := &models.Book{}
// 	err := row.Scan(&book.Id, &book.Name, &book.Slug)
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return book, nil
// }
