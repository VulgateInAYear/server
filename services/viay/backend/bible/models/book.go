package models

type Book struct {
	Id int32 `json:"-"`
	BibleId int32 `json:"-"`
	Number int16 `json:"Number"`
	Name string `json:"Name"`
	Slug string `json:"Slug"`
	Chapters int16 `json:"Chapters"`
}
