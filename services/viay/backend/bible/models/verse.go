package models

type Verse struct {
	Id int32 `json:"-"`
	ChapterId int32 `json:"-"`
	Number int16 `json:"Number"`
	Content string `json:"Content"`
}
