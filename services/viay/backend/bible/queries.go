package bible

import (
	"context"

	"viay/bible/models"
)

func (self *Bible) RetrieveBibles() ([]*models.Bible, error) {
	rows, err := self.Conn.Query(context.Background(), "SELECT Id, Name, Slug FROM Bibles;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bibles []*models.Bible
	for rows.Next() {
		bible := &models.Bible{}
		err := rows.Scan(&bible.Id, &bible.Name, &bible.Slug)
		if err != nil {
			return nil, err
		}
		bibles = append(bibles, bible)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return bibles, nil
}

func (self *Bible) RetrieveBible(slug string) (*models.Bible, error) {
	row := self.Conn.QueryRow(context.Background(), "SELECT Id, Name, Slug FROM Bibles WHERE Slug=$1;", slug)
	bible := &models.Bible{}
	err := row.Scan(&bible.Id, &bible.Name, &bible.Slug)
	if err != nil {
		return nil, err
	}

	return bible, nil
}

func (self *Bible) RetrieveBooks(bibleSlug string) ([]*models.Book, error) {
	rows, err := self.Conn.Query(context.Background(), "SELECT Id, Number, Name, Slug, Chapters FROM Books WHERE BibleId=(SELECT Id FROM Bibles WHERE Slug=$1);", bibleSlug)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var books []*models.Book
	for rows.Next() {
		book := &models.Book{}
		err := rows.Scan(&book.Id, &book.Number, &book.Name, &book.Slug, &book.Chapters)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return books, nil
}

func (self *Bible) RetrieveBook(bibleSlug string, bookSlug string) (*models.Book, error) {
	row := self.Conn.QueryRow(context.Background(), "SELECT Id, Number, Name, Slug, Chapters FROM Books WHERE Slug=$1 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$2);", bookSlug, bibleSlug)
	book := &models.Book{}
	err := row.Scan(&book.Id, &book.Number, &book.Name, &book.Slug, &book.Chapters)
	if err != nil {
		return nil, err
	}

	return book, nil
}

func (self *Bible) RetrieveChapters(bibleSlug string, bookSlug string) ([]*models.Chapter, error) {
	rows, err := self.Conn.Query(context.Background(), "SELECT Id, Number, COALESCE(Name, '') as Name FROM Chapters WHERE BookId=(SELECT Id FROM Books WHERE SLUG=$1 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$2)) ORDER BY Number ASC;", bookSlug, bibleSlug)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var chapters []*models.Chapter
	for rows.Next() {
		chapter := &models.Chapter{}
		err := rows.Scan(&chapter.Id, &chapter.Number, &chapter.Name)
		if err != nil {
			return nil, err
		}
		chapters = append(chapters, chapter)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return chapters, nil
}

func (self *Bible) RetrieveChapter(bibleSlug string, bookSlug string, chapterNumber string) (*models.Chapter, error) {
	row := self.Conn.QueryRow(context.Background(), "SELECT Id, Number, COALESCE(Name, '') as Name FROM Chapters WHERE Number=$1 AND BookId=(SELECT Id FROM Books WHERE Slug=$2 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$3));", chapterNumber, bookSlug, bibleSlug)
	chapter := &models.Chapter{}
	err := row.Scan(&chapter.Id, &chapter.Number, &chapter.Name)
	if err != nil {
		return nil, err
	}

	return chapter, nil
}

func (self *Bible) RetrieveVerses(bibleSlug string, bookSlug string, chapterNumber string) ([]*models.Verse, error) {
	rows, err := self.Conn.Query(context.Background(), "SELECT Id, Number, COALESCE(Content, '') as Content FROM Verses WHERE ChapterId=(SELECT Id as Name FROM Chapters WHERE Number=$1 AND BookId=(SELECT Id FROM Books WHERE Slug=$2 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$3))) ORDER BY Number ASC;", chapterNumber, bookSlug, bibleSlug)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var verses []*models.Verse
	for rows.Next() {
		verse := &models.Verse{}
		err := rows.Scan(&verse.Id, &verse.Number, &verse.Content)
		if err != nil {
			return nil, err
		}
		verses = append(verses, verse)
	}

	if rows.Err() != nil {
		return nil, rows.Err()
	}

	return verses, nil
}

func (self *Bible) RetrieveVerse(bibleSlug string, bookSlug string, chapterNumber string, verseNumber string) (*models.Verse, error) {
	row := self.Conn.QueryRow(context.Background(), "SELECT Id, Number, COALESCE(Content, '') as Content FROM Verses WHERE Id=$1 AND ChapterId=(SELECT Id as Name FROM Chapters WHERE Number=$2 AND BookId=(SELECT Id FROM Books WHERE Slug=$3 AND BibleId=(SELECT Id FROM Bibles WHERE Slug=$4)));", verseNumber, chapterNumber, bookSlug, bibleSlug)
	verse := &models.Verse{}
	err := row.Scan(&verse.Id, &verse.Number, &verse.Content)
	if err != nil {
		return nil, err
	}

	return verse, nil
}
