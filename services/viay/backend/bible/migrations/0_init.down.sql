ALTER TABLE Books DROP CONSTRAINT fk_bible;
ALTER TABLE Chapters DROP CONSTRAINT fk_book;
ALTER TABLE Verses DROP CONSTRAINT fk_chapter;
DROP TABLE Bibles, Books, Chapters, Verses;
