package bible

import (
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Bible struct {
	Router *mux.Router
	Conn *pgxpool.Pool
}

func (self *Bible) Route() {
	r := self.Router.PathPrefix("/bibles").Subrouter()
	r.HandleFunc("", self.getBibles)

	r.HandleFunc("/{bible}", self.getBible)
	r.HandleFunc("/{bible}/books", self.getBooks)

	r.HandleFunc("/{bible}/{book}", self.getBook)
 	r.HandleFunc("/{bible}/{book}/chapters", self.getChapters)

 	r.HandleFunc("/{bible}/{book}/{chapter:[0-9]+}", self.getChapter)
 	r.HandleFunc("/{bible}/{book}/{chapter:[0-9]+}/verses", self.getVerses)

 	r.HandleFunc("/{bible}/{book}/{chapter:[0-9]+}/{verse:[0-9]+}", self.getVerse)
}
