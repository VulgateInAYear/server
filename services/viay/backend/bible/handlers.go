package bible

import (
	"log"
	"net/http"
	"encoding/json"

	"github.com/gorilla/mux"
)

func (self *Bible) getBibles(w http.ResponseWriter, r *http.Request) {
	bibles, err := self.RetrieveBibles()
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(bibles)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getBible(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bible, err := self.RetrieveBible(vars["bible"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(bible)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getBooks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	books, err := self.RetrieveBooks(vars["bible"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(books)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	book, err := self.RetrieveBook(vars["bible"], vars["book"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(book)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getChapters(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	chapters, err := self.RetrieveChapters(vars["bible"], vars["book"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(chapters)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getChapter(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	chapter, err := self.RetrieveChapter(vars["bible"], vars["book"], vars["chapter"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(chapter)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getVerses(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	verses, err := self.RetrieveVerses(vars["bible"], vars["book"], vars["chapter"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(verses)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}

func (self *Bible) getVerse(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	verse, err := self.RetrieveVerse(vars["bible"], vars["book"], vars["chapter"], vars["verse"])
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	data, err := json.Marshal(verse)
	if err != nil {
		w.WriteHeader(500)
		log.Println(err.Error())
		return
	}

	w.Header().Set("Content-Type", "text/json")
	w.Write(data)
}
